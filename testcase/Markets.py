# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class MarketsInterFace(unittest.TestCase):
    #手机号搜索
    @get_whith_token("http://marketing.moxiancn.com:80/mo_marketing/m1/feedback/searchUserbyMobile,{companyId:1,shopId:1,mobile:1859193604}")
    def test_Params_GET_searchUserbyMobile(self,r):
        parse_status(r)

    #创建会员
    @post_with_token("http://marketing.moxiancn.com:80/mo_marketing/m1/feedback/creatMember,{'companyId':1,'shopId':1 ,'mobile':18591693604,'sex':0,'nickname':'NMLEE','birthday':25.0,'level':1,'backer':'Backup'}")
    def test_Params_POST_creatMember(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class CommentInterFace(unittest.TestCase):
    #应用评分
    @post_with_token("http://comment.moxiancn.com/mo_comment/m1/appscore,{'userId':userId,'appName':'1','appVersion':'1','telephoneType':'1','moxianVersion':'1'}")
    def test_Params_POST_appscore(self,r):
        parse_status(r)

    #发布文字和图片
    @post_with_token("http://comment.moxiancn.com/mo_comment/m1/feedbackopinion/insert,{'text': 'TEST','image':('http://image.moxiancn.com/certificate/99111432720234925.png'),'userId': userId,'telephoneType':'1','appVersion':'1','feedbackType':0}")
    def test_Params_POST_insert(self,r):
        parse_status(r)

    #店铺二维码
    @post_with_token("http://comment.moxiancn.com/mo_comment/m1/versionintroduce,{'languageType':'1','moxianVersion':'1'}")
    def test_Params_POST_versionintroduce(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
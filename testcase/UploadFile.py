# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class UploadFileInterFace(unittest.TestCase):
    #单文件上传
    @post_with_token("http://file.moxiancn.com/mo_common_fileuploadservice/m1/upload,{'file': '/','userId': userId,'fileType':1,'fileClassfycation':2}")
    def test_Params_POST_upload(self,r):
        parse_status(r)

    #同时长传多个同类型文件
    @post_with_token("http://file.moxiancn.com/mo_common_fileuploadservice/m1/upload/uploadFiles,{'file': '/','userId': userId,'fileType':1,'fileClassfycation':2}")
    def test_Params_POST_uploadFiles(self,r):
        parse_status(r)

    #域名查询
    @get_without_token("http://file.moxiancn.com:80/mo_common_fileuploadservice/m1/upload/getDomain,")
    def test_Noparams_GET_getDomain(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
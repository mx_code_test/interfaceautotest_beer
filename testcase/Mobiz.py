# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class MobizInterFace(unittest.TestCase):
    #魔商登录下一步
    @get_whith_token("http://mobiz.moxiancn.com/mo_biz/m1/merchant/getstep,")
    def test_Noparams_GET_getstep(self,r):
        parse_status(r)

    #魔商公司店铺首页
    @get_whith_token("http://mobiz.moxiancn.com/mo_biz/m1/company/index,")
    def test_Noparams_GET_index(self,r):
        parse_status(r)

    #店铺二维码
    @get_whith_token("http://mobiz.moxiancn.com/mo_biz/m1/shop/createqrcode,{shopId:10}")
    def test_Params_GET_createqrcode(self,r):
        parse_status(r)

    #创建公司
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/save,{'companyName':'Moxian_test','companyType':3,'contactNumber':'18682175998','contactName':'gyf','contactEmail':'123123@qq.com','countryCod':'86'}")
    def test_Params_POST_save(self,r):
        parse_status(r)

    #编辑公司
    @put_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/update,{'companyName':'Moxian_test','companyType':3,'contactNumber':'18682175998','contactName':'gyf','contactEmail':'123123@qq.com','countryCod':'86','addressDistrict':'shenzhen,guangdong province,china','addressDetail':'futian district'}")
    def test_Params_PUT_update(self,r):
        parse_status(r)

    #查询公司信息
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/search,{'companyId':0006} ")
    def test_Params_GET_search(self,r):
        parse_status(r)

    #删除公司
    @delete_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/delete?companyId=0006,")
    def test_Noparams_DELETE_delete(self,r):
        parse_status(r)

    #行业类型查询
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/industryCategory/nomerchant/getCategoryByPrId,{ 'pId': 3, 'countryCode': 'cn' }")
    def test_Params_GET_getCategoryByPrId(self,r):
        parse_status(r)

    #创建店铺
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/addshop,{ 'companyId': '7112','name': '食品店','logo': '图片','longitude':'112.564866','latitude': '22.654879','area': 1975,'detailedAddress': '深圳市岗厦中深大厦2305','hotline': '110755-7560811','introduce': '肉松饼店','backgroundUrl':'3333'}")
    def test_Params_POST_addshop(self,r):
        parse_status(r)

    #编辑店铺
    @put_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/updateshop,{'id':7112,'categoryId':1,'name':'食品店','logo':'/opt/imagedata/113.jpg','longitude':'112.564866','latitude':'22.654879','area':'2322132','detailedAddress':'深圳市岗厦中深大厦2305','hotline':'110755-7560811','introduce':'肉松饼店','takeoutHotline':'110755-7560811','reservationHotline':'110755-7560811','businessHoursBegin':1427795234,'businessHoursEnd':1427795234,'serve':'WIFI','personalityUrl':'http://www.baidu.com','backgroundUrl':'33333','isFullTime':1}")
    def test_Params_PUT_updateshop(self,r):
        parse_status(r)

    #编辑店铺服务
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/updateShopServices,{'shopId':1,'data':{'list':[{'id':0,'serviceId':1,'name':'外卖热线','activeFlag':'y'}],'reservationHotline':'111333','takeoutHotline':'33233232'}}")
    def test_Params_POST_updateShopServices(self,r):
        parse_status(r)

    #显示店铺信息
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant?id=7112,")
    def test_Noparams_GET_nomerchant(self,r):
        parse_status(r)

    #显示店铺营业时间
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getSaleTime?shopId=7112,")
    def test_Noparams_GET_getSaleTime(self,r):
        parse_status(r)

    #编辑店铺营业周期
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/updateSaleDay,{'shopId':7112,'saleDay': '1|2|3'}")
    def test_Params_POST_updateSaleDay(self,r):
        parse_status(r)

    #获取店铺服务
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getShopServices?shopId=7112,")
    def test_Noparams_GET_getShopServices(self,r):
        parse_status(r)

    #创建资质认证
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/createcompanyqualify, {'companyId': 7112, 'companyType': 3, 'licenseCode': '234232323', 'checkStatus': '1', 'idCard': '420800197012204530', 'idcardFrontUrl': '/idCardFront.png', 'idcardBackUrl': '/idCardBehind.png', 'contactNumber': '8615915343022', 'contactName': '张三', 'corporater': '张三', 'licensePicUrl': '/qualification.png'} ")
    def test_Params_POST_createcompanyqualify(self,r):
        parse_status(r)

    #获取资质认证信息
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/getcompanyqualifybyid?companyId=7112,")
    def test_Noparams_GET_getcompanyqualifybyid(self,r):
        parse_status(r)

    #修改资质认证信息
    @put_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/company/updatecompanyqualify,{ 'companyId': 7112, 'companyType': 3, 'licenseCode': '234232323', 'checkStatus': '1', 'idCard': '420800197012204530', 'idcardFrontUrl': '/idCardFront.png', 'idcardBackUrl': '/idCardBehind.png', 'contactNumber': '8615915343022', 'contactName': '张三', 'corporater': '张三', 'licensePicUrl': '/qualification.png'} ")
    def test_Params_PUT_updatecompanyqualify(self,r):
        parse_status(r)

    #魔商下所有门店
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getShopsByMerchantId?merchantId=CN4141&longitude=114.068634&latitude=22.531464,")
    def test_Noparams_GET_getShopsByMerchantId(self,r):
        parse_status(r)

    #分类查询店铺
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getShopsByCategoryId?categoryId=1,")
    def test_Noparams_GET_getShopsByCategoryId(self,r):
        parse_status(r)

    #推荐行业分类
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/industryCategory/nomerchant/recommendcategory?cityId=1973&countryCode=cn,")
    def test_Noparams_GET_recommendcategory(self,r):
        parse_status(r)

    #全部分类
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/industryCategory/nomerchant/allcategory?countryCode=cn,")
    def test_Noparams_GET_allcategory(self,r):
        parse_status(r)

    #查询通用门店数量
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getShopCountByGoodsId?goodsId=11,")
    def test_Noparams_GET_getShopCountByGoodsId(self,r):
        parse_status(r)

    #查询通用门店
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/searchShopsByGoods?goodsId=11&longitude=114.068634&latitude=22.531464,")
    def test_Noparams_GET_searchShopsByGoods(self,r):
        parse_status(r)

    #推荐商家
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getFecommendShops,{'cityCode': '0|1948|1973', 'longitude': 133.0154, 'latitude': 22.1234, 'page':1, 'limit':20}")
    def test_Params_POST_getFecommendShops(self,r):
        parse_status(r)

    #店铺推荐商品
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/getrecommendgoods?shopId=1&longitude=133.0154&latitude=22.1234,")
    def test_Noparams_GET_getrecommendgoods(self,r):
        parse_status(r)

    #店铺首页
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/shop/nomerchant/shopHomePage?id=15&longitude=113.254&latitude=33.1216,")
    def test_Noparams_GET_shopHomePage(self,r):
        parse_status(r)

    #我的店铺-添加员工
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/insertEmployee,{'merchantId': 7112,'companyId': 7112,'shopId': 7112,'userId': 7112,'employeeUserId': 1,'employeeRoleId': 1,'name': 'gyf1','telephone': '18658595442','sex': 0,'remark': '111','roleId': 1,'roleCode': 1,'status': 1,'isReceiving': 1,'opr': 1,'searchTelephone': '854854552','searchRoleCode': '12','offset': 12,'pageSize': 1}")
    def test_Params_POST_insertEmployee(self,r):
        parse_status(r)

    #我的店铺-编辑员工
    @put_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/updateEmployee,{'merchantId': 7112,'companyId': 7112,'shopId': 0,'userId': 0,'employeeUserId': 115,'employeeRoleId': 1,'name': '','telephone': '','sex': 0,'remark': '','roleId': 0,'roleCode': 0,'status': 0,'isReceiving': 0,'opr': 0,'searchTelephone': '','searchRoleCode': '','offset': 0,'pageSize': 0}")
    def test_Params_PUT_updateEmployee(self,r):
        parse_status(r)

    #我的店铺-查询员工列表
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/searchEmployeeList?shopId=1&status=1&pageIndex=1,")
    def test_Noparams_GET_searchEmployeeList(self,r):
        parse_status(r)

    #我的店铺-查询员工详情
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/getEmployeeById?employeeRoleId=12,")
    def test_Noparams_GET_getEmployeeById(self,r):
        parse_status(r)

    #我的店铺-查询在职店铺列表
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/getEmployeeShopList?shopId=1&companyId=1&employeeUserId=1&pageIndex=1,")
    def test_Noparams_GET_getEmployeeShopList(self,r):
        parse_status(r)

    #我的店铺-设置员工接收消息
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/setCustomService?shopId=1&employeeUserId=12,")
    def test_Noparams_GET_setCustomService(self,r):
        parse_status(r)

    #我的店铺-扫搜魔线用户
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/employee/searchMxUser?telephoneOrMxId=11113&companyId=1172&shopId=1,")
    def test_Noparams_GET_searchMxUser(self,r):
        parse_status(r)

    #我的公司-获取公司管理员列表
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/admin/getadminlist?companyId=112&statusIType=1,")
    def test_Noparams_GET_getadminlist(self,r):
        parse_status(r)

    #我的公司-新增管理员
    @post_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/admin/createadminemployee,{'userId': 126, 'companyId': 143, 'phone': '18682175555', 'sex': 1, 'name': '奥巴马'}")
    def test_Params_POST_createadminemployee(self,r):
        parse_status(r)

    #我的公司-编辑管理员
    @put_with_token("http://mobiz.moxiancn.com:80/mo_biz/m1/admin/updateadminemployee,{'mxUserId': 127, 'companyId': 143, 'telePhone': '111185845', 'statusIType': 1, 'name': '奥巴马'} ")
    def test_Params_PUT_updateadminemployee(self,r):
        parse_status(r)

    #我的公司-获取管理员详情
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/admin/getadminemployee?mxUserId=127&companyId=1,")
    def test_Noparams_GET_getadminemployee(self,r):
        parse_status(r)

    #我的公司-搜索魔线用户
    @get_whith_token("http://mobiz.moxiancn.com:80/mo_biz/m1/admin/searchuser?companyId=1&userIdOrPhone=1,")
    def test_Noparams_GET_searchuser(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
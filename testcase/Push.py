# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class PushInterFace(unittest.TestCase):
    #保存用户推送设备信息
    @put_with_token("http://push.moxiancn.com/mo_common_push/m1/push/cid,{'cid' : 'b812c88235f23061b02876383369dce3', 'deviceType' : 1, 'appType': 1'}")
    def test_Params_PUT_cid(self,r):
        parse_status(r)

    #对单个用户推送消息
    @post_with_token("http://push.moxiancn.com/mo_common_push/m1/push,{'toUserId': userId, 'messageType': 1, 'contentType': 1, 'message' : 'hello world! smile'}")
    def test_Params_POST_push(self,r):
        parse_status(r)

    #个推设备解绑
    @delete_with_token("http://push.moxiancn.com/mo_common_push/m1/push/unbindcid,{'userId': 23123, 'cid' : 'b812c88235f23061b02876383369dce3'}")
    def test_Params_DELETE_unbindcid(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
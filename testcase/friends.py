# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class friendsInterFace(unittest.TestCase):
    #加关注
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/123,")
    def test_Noparams_POST_fans_123(self,r):
        parse_status(r)

    #取消关注
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/1,")
    def test_Noparams_DELETE_fans_1(self,r):
        parse_status(r)

    #Delete location by userId
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans,")
    def test_Noparams_DELETE_fans3(self,r):
        parse_status(r)

    #According to the condition of accurate search user
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/users,{condition:s}")
    def test_Params_GET_users(self,r):
        parse_status(r)

    #Get user by id
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/user/42,")
    def test_Noparams_GET_user_42(self,r):
        parse_status(r)

    #search phone book
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/phones,{'phones':'18591693604'}")
    def test_Params_POST_phones(self,r):
        parse_status(r)

    #find near friends
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/nearby/people?page=1&size=5,{'x': 0,'y': 0,'gender': 0,'hour': 0,'distance': 100}")
    def test_Params_POST_people(self,r):
        parse_status(r)

    #following Query user following list
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/44/following,")
    def test_Noparams_GET_following(self,r):
        parse_status(r)

    #Query user follower list
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/44/follower,")
    def test_Noparams_GET_follower(self,r):
        parse_status(r)

    #修改备注
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/remark,{'content': 'ddddd','userId': 1}")
    def test_Params_POST_remark(self,r):
        parse_status(r)

    #fuzzy query
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/search?condition=22,")
    def test_Noparams_GET_search(self,r):
        parse_status(r)

    #Create a new notify
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/notify,{'friendId': 1,'notity': False}")
    def test_Params_POST_notify(self,r):
        parse_status(r)

    #好友列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans,")
    def test_Noparams_GET_fans(self,r):
        parse_status(r)

    #Get user details
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/42,")
    def test_Noparams_GET_fans_42(self,r):
        parse_status(r)

    #粉丝列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/my/follower,")
    def test_Noparams_GET_follower15(self,r):
        parse_status(r)

    #关注人列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/my/following,")
    def test_Noparams_GET_following16(self,r):
        parse_status(r)

    #Get fans count
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/count,")
    def test_Noparams_GET_count(self,r):
        parse_status(r)

    #保存位置
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/location,{'x': 121.505003,'y': 31.222907,'gender': 0,'visibility': True}")
    def test_Params_POST_location(self,r):
        parse_status(r)

    #Find group list
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/group,")
    def test_Noparams_GET_group(self,r):
        parse_status(r)

    #创建群组
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/group,{'name': 'friends'}")
    def test_Params_POST_group(self,r):
        parse_status(r)

    #删除群组
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/135,")
    def test_Noparams_DELETE_group_135(self,r):
        parse_status(r)

    #编辑组名
    @put_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/136,{'name': 'friendgyf2'}")
    def test_Params_PUT_group_136(self,r):
        parse_status(r)

    #Get group by id
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/group/136,")
    def test_Noparams_GET_group_136(self,r):
        parse_status(r)

    #添加成员
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/136/addFriends,{'fids': [2]}")
    def test_Params_POST_addFriends(self,r):
        parse_status(r)

    #删除成员
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/136/removeFriends,{'fids': [2]}")
    def test_Params_POST_removeFriends(self,r):
        parse_status(r)

    #查询组好友列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/group/136/friends,")
    def test_Noparams_GET_friends(self,r):
        parse_status(r)

    #未加组好友
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/group/136/notincurrentfriends,")
    def test_Noparams_GET_notincurrentfriends(self,r):
        parse_status(r)

    #添加成员到多个组
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/addFriendsToMultiple,{'gids': [136],'friendId': 1}")
    def test_Params_POST_addFriendsToMultiple(self,r):
        parse_status(r)

    #查询好友已添加的组
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/group/136/friendgroups,")
    def test_Noparams_POST_friendgroups(self,r):
        parse_status(r)

    #加入黑名单
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/black/136,")
    def test_Noparams_POST_black_136(self,r):
        parse_status(r)

    #移除黑名单
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/fans/black/136,")
    def test_Noparams_DELETE_black_136(self,r):
        parse_status(r)

    #黑名单列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/fans/black,")
    def test_Noparams_GET_black(self,r):
        parse_status(r)

    #举报用户
    @post_with_token("http://pal.moxiancn.com:80/mo_pal/api/report,{'typeIds': [1],'user': '123123','content': '321321'}")
    def test_Params_POST_report(self,r):
        parse_status(r)

    #举报类型
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/report/options,")
    def test_Noparams_GET_options(self,r):
        parse_status(r)

    #Get Report by Id
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/report/2/findone,")
    def test_Noparams_GET_findone(self,r):
        parse_status(r)

    #Delete Report by Id
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/report/2,")
    def test_Noparams_DELETE_report_2(self,r):
        parse_status(r)

    #Get Report by userId
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/report/42,")
    def test_Noparams_GET_report_42(self,r):
        parse_status(r)

    #消息列表
    @get_whith_token("http://pal.moxiancn.com:80/mo_pal/api/message,")
    def test_Noparams_GET_message(self,r):
        parse_status(r)

    #删除消息
    @delete_with_token("http://pal.moxiancn.com:80/mo_pal/api/message/12,")
    def test_Noparams_DELETE_message_12(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
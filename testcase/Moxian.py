# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class MoxianInterFace(unittest.TestCase):
    #获取个人资料
    @get_whith_token("http://moxian.moxiancn.com:80/mo_moxian/m1/userprofile,")
    def test_Noparams_GET_userprofile(self,r):
        parse_status(r)

    #set language
    @post_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/userprofile/setlanguage,{'userId':userId,'language':'zhong'}")
    def test_Params_POST_setlanguage(self,r):
        parse_status(r)

    #Get system tags
    @get_whith_token("http://moxian.moxiancn.com:80/mo_moxian/m1/userprofile/systemtagsinfo?countryCode=123,")
    def test_Noparams_GET_systemtagsinfo(self,r):
        parse_status(r)

    #Edit User Base info
    @put_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/userprofile/baseinfo,{'birthday': 50,'tags': 'java','school': 'yantai','major': 'IT', 'type': 6,'userId':1010105829}")
    def test_Params_PUT_baseinfo(self,r):
        parse_status(r)

    #Get Avatar by user ID
    @get_whith_token("http://moxian.moxiancn.com:80/mo_moxian/m1/avatar/getavatar,")
    def test_Noparams_GET_getavatar(self,r):
        parse_status(r)

    #getavatardetail Get Avatar by user ID
    @get_whith_token("http://moxian.moxiancn.com:80/mo_moxian/m1/avatar/getavatardetail?avatarId=123,")
    def test_Noparams_GET_getavatardetail(self,r):
        parse_status(r)

    #set defaultavatar
    @post_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/avatar/setavatar,{'avatarId': 0,'userId': userId}")
    def test_Params_POST_setavatar(self,r):
        parse_status(r)

    #uploadavatar SetAvatarResource
    @post_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/avatar/uploadavatar,{'avatarId': 0,'avatarPath': '/'}")
    def test_Params_POST_uploadavatar(self,r):
        parse_status(r)

    #Delete a user avatar
    @delete_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/avatar/delete?avatarId=123,")
    def test_Noparams_DELETE_delete(self,r):
        parse_status(r)

    #Create an address
    @post_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/deliveryaddress,{'id': 0,'address1': '景新花园','recipients': '晓龙','phoneNo': '18591693604','postcode': ''}")
    def test_Params_POST_deliveryaddress(self,r):
        parse_status(r)

    #update an address
    @put_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/deliveryaddress,{'id': 0,'address1': '景新花园','recipients': '晓龙','areaCode': '','areaName': '','phoneNo': '','postcode': ''}")
    def test_Params_PUT_deliveryaddress(self,r):
        parse_status(r)

    #Delete an address
    @delete_with_token("http://moxian.moxiancn.com:80/mo_moxian/m1/deliveryaddress?addressId=45,")
    def test_Noparams_DELETE_deliveryaddress(self,r):
        parse_status(r)

    #Get all address by userId
    @get_whith_token("http://moxian.moxiancn.com:80/mo_moxian/m1/deliveryaddress,")
    def test_Noparams_GET_deliveryaddress(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
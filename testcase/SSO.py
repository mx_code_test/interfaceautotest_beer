# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class SSOInterFace(unittest.TestCase):
    #banding your mobilephone here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/mobilephone/bandmobilephone,{'userId': userId,'email': '','validateCode': 'mxxx','countryCode': '86','phoneNo': '8613682613940'}")
    def test_Params_POST_bandmobilephone(self,r):
        parse_status(r)

    #update your mobilephone here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/mobilephone/updatemobilephone,{'userId': 1010105829,'email': '962484758@qq.com','validateCode': 'xxxx','countryCode': '86','phoneNo': '18591693604'}")
    def test_Params_POST_updatemobilephone(self,r):
        parse_status(r)

    #update your mobilephone here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/mobilephone/sendmessage,{'userId': 1010105829,'email': '','validateCode': '','countryCode': '86','phoneNo': '8618682175998'}")
    def test_Params_POST_sendmessage(self,r):
        parse_status(r)

    #Check phoneNo and send captchaCode message
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/reg/checkphoneno,{'countryCode': '86','phoneNo': '8618491693604'}")
    def test_Params_POST_checkphoneno(self,r):
        parse_status(r)

    #Regist an abstractUser
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/reg/moxian,{'nickName': '','password': '123123123','sexType': 0,'captcha': '0','phoneNo': '8618551693604','birthday': 1001,'birthdayViableType': 0,'countryCode': '86','city': '','isAdminItype': 0,'isMobizItype': 0,'isMopalItype': 0}")
    def test_Params_POST_moxian(self,r):
        parse_status(r)

    #register by phone number or email
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/reg/mobiz,{'account': '8618591693604','countryCode': '86','captcha': '1','password': '123123'}")
    def test_Params_POST_mobiz(self,r):
        parse_status(r)

    #forget password sendResetCode
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/forget/sendforgetcaptcha,{'type': 2,'mobile': '8618591693604','email': '962484758@qq.com'}")
    def test_Params_POST_sendforgetcaptcha(self,r):
        parse_status(r)

    #forget password checkResetCode
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/forget/checkcaptcha,{'code': 'xiaolong','type': 2,'mobile': '8618591693604','email': ''}")
    def test_Params_POST_checkcaptcha(self,r):
        parse_status(r)

    #forget password resetPassword
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/forget/resetpassword,{'param': '','newpassword': 'xiaolong','code': 'xxxx'}")
    def test_Params_POST_resetpassword(self,r):
        parse_status(r)

    #sending email here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/email/sendemail,{'userId': userId,'email': '962484758@qq.com','validateCode': '','countryCode': '','phoneNo': ''}")
    def test_Params_POST_sendemail(self,r):
        parse_status(r)

    #banding your email here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/email/bandemail,{'userId': userId,'email': '962484758@qq.com','validateCode': '123123','countryCode': '86','phoneNo': '18591693604'}")
    def test_Params_POST_bandemail(self,r):
        parse_status(r)

    #update user password here
    @post_with_token("http://sso.moxiancn.com:80/mo_common_sso/m1/updateuserpwd,{'userId': userId,'password': 'xiaolong','newPassword': 'xiaolong'}")
    def test_Params_POST_updateuserpwd(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
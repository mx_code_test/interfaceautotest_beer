# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class AdInterFace(unittest.TestCase):
    #启动页
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/initpage,{city:1973}")
    def test_Params_GET_initpage(self,r):
        parse_status(r)

    #魔商启动页
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/initpage/getMoBizInitPage,{city:0006,pageVersion:23142}")
    def test_Params_GET_getMoBizInitPage(self,r):
        parse_status(r)

    #获取Banner广告
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/ad/getbanner,{cityCode:1973,bannerCode:MARKET_BANNER}")
    def test_Params_GET_getbanner(self,r):
        parse_status(r)

    #获取推荐活动
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/location/getcitycode,{city:shenzhen}")
    def test_Params_GET_getcitycode(self,r):
        parse_status(r)

    #通过城市名获取对应城市CODE
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/ad/getactivity,{cityCode:1973,activityCode:MARKET_ACTIVITY}")
    def test_Params_GET_getactivity(self,r):
        parse_status(r)

    #获取开放城市列表
    @get_without_token("http://ad.moxiancn.com/mo_ad/m1/location/getopencity,")
    def test_Noparams_GET_getopencity(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
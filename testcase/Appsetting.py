# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class AppsettingInterFace(unittest.TestCase):
    #Get User Message Remind
    @get_whith_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/messageremind?appType=1,")
    def test_Noparams_GET_messageremind(self,r):
        parse_status(r)

    #set message remind
    @put_with_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/messageremind,{'id': 123,'userId':1010105829,'isRemindOpen': 0,'disturb': 0,'startTime': 0,'endTime': 0,'sound': 0,'vibration': 0,'appType': 1,'createBy': 0,'createAt': 0,'updateBy': 0,'updateTime': 0,'activeFlag': ''}")
    def test_Params_PUT_messageremind(self,r):
        parse_status(r)

    #check app version without login
    @get_whith_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/merchantsetting/norestfilter/checkversion?versionNo=2&appType=1,")
    def test_Noparams_GET_checkversion(self,r):
        parse_status(r)

    #check app version
    @get_whith_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/merchantsetting/getnewversion?versionNo=2&appType=1,")
    def test_Noparams_GET_getnewversion(self,r):
        parse_status(r)

    #Create new 3rd party accredit record
    @post_with_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/thirdParty,{'id': 3,'thirdPartyId': 1,'thirdPartyLoginAccount': 'asdasd','thirdPartyName': 'Facebook','thirdPartyDb': '1','appType': 2,'activeFlag': ''}")
    def test_Params_POST_thirdParty(self,r):
        parse_status(r)

    #update 3rd party accredit record
    @put_with_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/thirdParty,{'id': 2,'thirdPartyId': 1,'thirdPartyLoginAccount': '123','thirdPartyName': 'Facebook','thirdPartyDb': 'moxian','appType':1,'activeFlag': '1'}")
    def test_Params_PUT_thirdParty(self,r):
        parse_status(r)

    #get all 3rd party accredit record by user id
    @get_whith_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/thirdParty?appType=2,")
    def test_Noparams_GET_thirdParty(self,r):
        parse_status(r)

    #delete a 3rd party accredit record
    @delete_with_token("http://setting.moxiancn.com:80/mo_common_appsetting/m1/thirdParty?recordId=1,")
    def test_Noparams_DELETE_thirdParty(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
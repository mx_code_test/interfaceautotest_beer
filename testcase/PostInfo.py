# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class PostInfoInterFace(unittest.TestCase):
    #Get Report by userId
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/report/42,")
    def test_Noparams_GET_report_42(self,r):
        parse_status(r)

    #Delete Report by Id
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/report/42,")
    def test_Noparams_DELETE_report_42(self,r):
        parse_status(r)

    #Create a new report
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/report,{'typeIds': [1],'post': 1,'content': 'EROTICISM'}")
    def test_Params_POST_report(self,r):
        parse_status(r)

    #Get Report by Id
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/report/42/findone,")
    def test_Noparams_GET_findone(self,r):
        parse_status(r)

    #Access options
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/report/options,")
    def test_Noparams_GET_options(self,r):
        parse_status(r)

    #Create a new Favorites
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/favorites,{'postId': 42}")
    def test_Params_POST_favorites(self,r):
        parse_status(r)

    #Get favorites attention dynamic
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/favorites?begin=1&end=4,")
    def test_Noparams_GET_favorites(self,r):
        parse_status(r)

    #Delete Favorites by Id
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/favorites/42,")
    def test_Noparams_DELETE_favorites_42(self,r):
        parse_status(r)

    #Delete Favorites by Id
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/favorites,")
    def test_Noparams_DELETE_favorites9(self,r):
        parse_status(r)

    #Create a new PostInfo
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo,{'post': 1,'user': 42,'content': 'testtest','type': 1}")
    def test_Params_POST_postinfo(self,r):
        parse_status(r)

    #Delete PostInfo by User
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo,")
    def test_Noparams_DELETE_postinfo(self,r):
        parse_status(r)

    #Get PostInfo by User
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo?begin=1&end=6&type=1,")
    def test_Noparams_GET_postinfo(self,r):
        parse_status(r)

    #Get PostInfo by Id
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo/1/findone,")
    def test_Noparams_GET_findone13(self,r):
        parse_status(r)

    #Get PostInfo by Post
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo/2,")
    def test_Noparams_GET_postinfo_2(self,r):
        parse_status(r)

    #Delete PostInfo by Post
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/postinfo/1,")
    def test_Noparams_DELETE_postinfo_1(self,r):
        parse_status(r)

    #Get PostResource by postId
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/postresource/1,")
    def test_Noparams_GET_postresource_1(self,r):
        parse_status(r)

    #Get Comment by postId
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/comment/2,")
    def test_Noparams_GET_comment_2(self,r):
        parse_status(r)

    #Get Comment by Id
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/comment/1/findone,")
    def test_Noparams_GET_findone18(self,r):
        parse_status(r)

    #Delete Comment by Id
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/comment/2,")
    def test_Noparams_DELETE_comment_2(self,r):
        parse_status(r)

    #Create a new comment
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/comment,{'post': 1,'replyComment': 1,'content': 'tttttxxxxx'}")
    def test_Params_POST_comment(self,r):
        parse_status(r)

    #Get star user
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/post/star/1?begin=0&end=4,")
    def test_Noparams_GET_1(self,r):
        parse_status(r)

    #Delete post by Id
    @delete_with_token("http://moment.moxiancn.com:80/mo_moment/api/post/1,")
    def test_Noparams_DELETE_post_1(self,r):
        parse_status(r)

    #Dynamic content Modify
    @put_with_token("http://moment.moxiancn.com:80/mo_moment/api/post/42,{'content': 'ttteeesssttt','latitude': 0,'longitude': 0,'forwardId': 1,'type': 0,'address': 'ddd','gids': [0],'fids': [0],'area': '','postResourceVo': [{'resourceValue': '1111'}]}")
    def test_Params_PUT_post_42(self,r):
        parse_status(r)

    #Get post by id
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/post/42,")
    def test_Noparams_GET_post_42(self,r):
        parse_status(r)

    #Dynamic cancel some praise
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/post/42/unstar,")
    def test_Noparams_POST_unstar(self,r):
        parse_status(r)

    #Create a new post
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/post,{'content': 'asdfadf','latitude': 0,'longitude': 0,'forwardId': 0,'type': 0,'address': '123123','gids': [0],'fids': [0],'area': '1111','postResourceVo': [{'resourceValue': '321321'}]}")
    def test_Params_POST_post26(self,r):
        parse_status(r)

    #Get attention dynamic
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/post?begin=0&end=4,")
    def test_Noparams_GET_post(self,r):
        parse_status(r)

    #Get nearby attention dynamic
    @get_whith_token("http://moment.moxiancn.com:80/mo_moment/api/post/nearby?begin=0&end=4&area=2,")
    def test_Noparams_GET_nearby(self,r):
        parse_status(r)

    #The dynamic point of praise
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/post/42/star,")
    def test_Noparams_POST_star(self,r):
        parse_status(r)

    #Shielding dynamic
    @post_with_token("http://moment.moxiancn.com:80/mo_moment/api/post/42/ignore,")
    def test_Noparams_POST_ignore(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
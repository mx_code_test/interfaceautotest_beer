# -*- coding: utf-8 -*-

import unittest
import json
import requests
import sys
import logging
sys.path.append('..')
from lib.statuscode import parse_status
from lib.log import *
from lib.method_decorator import *

class MogoodsInterFace(unittest.TestCase):
    #search goods category by id and level
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/goodsCategory,{'level': 0,'categoryId': 123124}")
    def test_Params_POST_goodsCategory(self,r):
        parse_status(r)

    #Get activity goods by module ID
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/nomerchant/hotkeywords?cityCode=001,")
    def test_Noparams_GET_hotkeywords(self,r):
        parse_status(r)

    #getShopGoodsDisplayList
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsdisplay/nomerchant/getshopgoodsdisplaylist?shopId=1&goodsType=1&pageIndex=1,")
    def test_Noparams_GET_getshopgoodsdisplaylist(self,r):
        parse_status(r)

    #get ShopGoods Detail By Id
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsdisplay/nomerchant/getshopgoodsdetailbyid?goodsId=1&activityId=1&shopId=1&longitude=1&latitude=1,")
    def test_Noparams_GET_getshopgoodsdetailbyid(self,r):
        parse_status(r)

    #getShopGoodsById
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/getShopGoodsById?goodsId=1&shopId=11&longitude=1&latitude=1,")
    def test_Noparams_GET_getShopGoodsById(self,r):
        parse_status(r)

    #getShopGoodsList
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/getShopGoodsList?shopId=1&pageIndex=1&goodsSummary=1&goodsStatus=1&goodsType=1,")
    def test_Noparams_GET_getShopGoodsList(self,r):
        parse_status(r)

    #getUpDownNumber
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/getUpDownNumber?shopId=1,")
    def test_Noparams_GET_getUpDownNumber(self,r):
        parse_status(r)

    #get shop homePage goods
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/nomerchant/homePageGoods?shopId=1,")
    def test_Noparams_GET_homePageGoods(self,r):
        parse_status(r)

    #get shop homePage goods
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/norestfilter/shopPreviewGoods?shopId=1,")
    def test_Noparams_GET_shopPreviewGoods(self,r):
        parse_status(r)

    #getShopGoodsCouponTemplet
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/getShopGoodsCouponTemplet?templetType=1&companyId=1&pageIndex=1,")
    def test_Noparams_GET_getShopGoodsCouponTemplet(self,r):
        parse_status(r)

    #saveShopGoods
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/saveShopGoods,{'userId': userId,'shopId': 1,'merchantId': 0,'goodsId': 0,'goodsCode': '1','companyId': 0,'goodsName': '1','goodsType': 1,'goodsSummary': '1','goodsDetail': '1','goodsStock': 1,'goodsGroupId': 1,'goodsUpTime': 1,'goodsDownTime': 1,'systemUpTime': 12,'systemDownTime': 123,'goodsRule': '','goodsStartTime': 1,'goodsEndTime': 0,'isRecommend': 0,'goodsBuyMax': 0,'goodsPictureJson': '','price': 0,'primePrice': 0,'sendMethod': '','postage': 0,'couponType': 0,'goodsCategoryId': '1','goodsCategoryName': '','isSpecial': 0,'specialPrice': 0,'specialStartTime': 0,'specialEndTime': 0,'specialBuyMax': 0,'offset': 0,'pageSize': 0,'logoUrl': '','templetType': '','goodsStatus': 0,'currency': '','couponPrice': 0,'goodsCount': 0,'searchGoodsGroupId': '','countryCode': ''}")
    def test_Params_POST_saveShopGoods(self,r):
        parse_status(r)

    #updateShopGoods
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/updateShopGoods,{'userId': userId,'shopId': 1,'merchantId': 0,'goodsId': 1,'goodsCode': '','companyId': 0,'goodsName': '1','goodsType': 1,'goodsSummary': '1','goodsDetail': '1','goodsStock': 1,'goodsGroupId': 0,'goodsUpTime': 123123,'goodsDownTime': 123123123,'systemUpTime': 0,'systemDownTime': 0,'goodsRule': '1','goodsStartTime': 111,'goodsEndTime': 11,'isRecommend': 0,'goodsBuyMax': 0,'goodsPictureJson': '','price': 0,'primePrice': 0,'sendMethod': '','postage': 0,'couponType': 0,'goodsCategoryId': '1','goodsCategoryName': '1','isSpecial': 0,'specialPrice': 0,'specialStartTime': 0,'specialEndTime': 0,'specialBuyMax': 0,'offset': 0,'pageSize': 0,'logoUrl': '','templetType': '','goodsStatus': 0,'currency': '1','couponPrice': 0,'goodsCount': 0,'searchGoodsGroupId': '','countryCode': ''}")
    def test_Params_PUT_updateShopGoods(self,r):
        parse_status(r)

    #getCurrency
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoods/getCurrency?shopId=1,")
    def test_Noparams_GET_getCurrency(self,r):
        parse_status(r)

    #Get activity classification by module ID
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/activityclassification/classifybymoduleid?moduleId=123,")
    def test_Noparams_GET_classifybymoduleid(self,r):
        parse_status(r)

    #Get activity goods by module ID
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/activitygoods/goodsbymoduleid?moduleId=321,")
    def test_Noparams_GET_goodsbymoduleid(self,r):
        parse_status(r)

    #Get activity goods by classify ID
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/activitygoods/goodsbyclassifyid?classifyId=2222,")
    def test_Noparams_GET_goodsbyclassifyid(self,r):
        parse_status(r)

    #Get shop goods group
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup/getgroups?shopId=123,")
    def test_Noparams_GET_getgroups(self,r):
        parse_status(r)

    #Update shop goods group
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup,{'id': userId,'shopId': 1,'name': '','order': 0,'createBy': 0,'createTime': '','updateBy': 0,'updateTime': '','activeFlag': '','goodsCount': 0}")
    def test_Params_PUT_shopgoodsgroup(self,r):
        parse_status(r)

    #Delete shop goods group
    @delete_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup?shopId=122&groupId=12,")
    def test_Noparams_DELETE_shopgoodsgroup(self,r):
        parse_status(r)

    #Create shop goods group
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup,{'id': 0,'shopId': 0,'name': '','order': 0,'createBy': 0,'createTime': '','updateBy': 0,'updateTime': '','activeFlag': '','goodsCount': 0}")
    def test_Params_POST_shopgoodsgroup(self,r):
        parse_status(r)

    #Get shop goods by groupId
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup/getshopgoods?groupId=1&shopId=1&pageIndex=1&pageSize=20,")
    def test_Noparams_GET_getshopgoods(self,r):
        parse_status(r)

    #add shop goods to group
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsgroup/addgoodstogroup,{'shopId': 11,'oldGroupId': 1,'newGroupId': 2,'goodsidArray': '11'}")
    def test_Params_PUT_addgoodstogroup(self,r):
        parse_status(r)

    #Get shop total
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/nomerchant/goodssearch/category,{'cityCode': '11','type': 1,'categoryId': 0,'longitude': 0,'latitude': 0,'radius': 0,'onlyMofen': 0,'haveProducts': 0,'page': 0,'limit': 0}")
    def test_Params_POST_category(self,r):
        parse_status(r)

    #Get activity goods by module ID
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/nomerchant/goodssearch/location,{'cityCode': '11','type': 1,'categoryId': 0,'longitude': 0,'latitude': 0,'radius': 0,'onlyMofen': 0,'haveProducts': 0,'page': 0,'limit': 0}")
    def test_Params_POST_location(self,r):
        parse_status(r)

    #Get activity goods by module ID
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/nomerchant/goodssearch/coupon,{'cityCode': '11','type': 0,'categoryId': 0,'longitude': 0,'latitude': 0,'radius': 0,'onlyMofen': 0,'haveProducts': 0,'page': 0,'limit': 0}")
    def test_Params_POST_coupon(self,r):
        parse_status(r)

    #Get activity goods by module ID
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/nomerchant/goodssearch,{'cityCode': '1','type': 0,'categoryId': 0,'longitude': 0,'latitude': 0,'radius': 0,'onlyMofen': 0,'haveProducts': 0,'page': 0,'limit': 0}")
    def test_Params_POST_goodssearch(self,r):
        parse_status(r)

    #get attention shop goods and dynamic
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/mallattentionresource/nomerchant?pageIndex=1&longitude=1&latitude=1,")
    def test_Noparams_GET_nomerchant(self,r):
        parse_status(r)

    #get shop coupon
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/shopCoupon/savecoupon?goodsId=11,")
    def test_Noparams_GET_savecoupon(self,r):
        parse_status(r)

    #add visitcnt
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsdynamic/nomerchant/shopgoodsdynamic/addvisitcnt,{'shopId': 123,'goodsId': 1}")
    def test_Params_PUT_addvisitcnt(self,r):
        parse_status(r)

    #add collection count
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsdynamic/nomerchant/shopgoodsdynamic/addcollectioncnt,{'shopId': 1,'goodsId': 1}")
    def test_Params_PUT_addcollectioncnt(self,r):
        parse_status(r)

    #reduce collection count
    @put_with_token("http://goods.moxiancn.com:80/mo_goods/m1/shopgoodsdynamic/nomerchant/shopgoodsdynamic/reducecollectioncnt,{'shopId': 1,'goodsId': 1}")
    def test_Params_PUT_reducecollectioncnt(self,r):
        parse_status(r)

    #get mofen scope list
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/mofenscope,")
    def test_Noparams_GET_mofenscope(self,r):
        parse_status(r)

    #save mofen scope
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/mofenscope,{'minValue': 1,'maxValue': 1}")
    def test_Params_POST_mofenscope(self,r):
        parse_status(r)

    #get found shop goods and dynamic
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/mallfoundresource/nomerchant?pageIndex=1&cityId=12,")
    def test_Noparams_GET_nomerchant(self,r):
        parse_status(r)

    #delete goods,set goods deleteflag
    @delete_with_token("http://goods.moxiancn.com:80/mo_goods/m1/managergoods/delete?shopId=1&goodsId=1,")
    def test_Noparams_DELETE_delete(self,r):
        parse_status(r)

    #off shelf goods
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/managergoods/offShelf,{'userId': userId,'shopId': 0,'merchantId': '','goodsId': 0,'goodsUpTime': 0,'goodsDownTime': 0}")
    def test_Params_POST_offShelf(self,r):
        parse_status(r)

    #on shelf goods
    @post_with_token("http://goods.moxiancn.com:80/mo_goods/m1/managergoods/onShelf,{'userId': userId,'shopId': 0,'merchantId': '','goodsId': 0,'goodsUpTime': 0,'goodsDownTime': 0}")
    def test_Params_POST_onShelf(self,r):
        parse_status(r)

    #query templet by goods type
    @get_whith_token("http://goods.moxiancn.com:80/mo_goods/m1/managergoods/queryTemplet?ruleType=123,")
    def test_Noparams_GET_queryTemplet(self,r):
        parse_status(r)

if __name__ == '__main__':
    initLogging('../log/output.log')
    unittest.main()
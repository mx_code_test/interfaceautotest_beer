#!/usr/bin/env python
# -*- coding:utf-8 -*-

__author__ = 'shi.xiaolong'

import json, functools
import requests
import logging
from statuscode import parse_status

_URL = 'http://login.moxiancn.com/mo_common_login/m1/auth/login'
query_args = {
    "useraccount": "8613682613940",
    "userpass": "111111"
}
_HEADERS = {'content-type': 'application/json'}

_OAUTH_ERROR = {
    '0001': 'login_failed',
    '0002': 'invalid_access_token',
    '0003': 'expired_token',
}

_RESPONSE_STATUSES = {
    # Informational
    100: 'Continue',
    101: 'Switching Protocols',
    102: 'Processing',

    # Successful
    200: 'OK',
    201: 'Created',
    202: 'Accepted',
    203: 'Non-Authoritative Information',
    204: 'No Content',
    205: 'Reset Content',
    206: 'Partial Content',
    207: 'Multi Status',
    226: 'IM Used',

    # Redirection
    300: 'Multiple Choices',
    301: 'Moved Permanently',
    302: 'Found',
    303: 'See Other',
    304: 'Not Modified',
    305: 'Use Proxy',
    307: 'Temporary Redirect',

    # Client Error
    400: 'Bad Request',
    401: 'Unauthorized',
    402: 'Payment Required',
    403: 'Forbidden',
    404: 'Not Found',
    405: 'Method Not Allowed',
    406: 'Not Acceptable',
    407: 'Proxy Authentication Required',
    408: 'Request Timeout',
    409: 'Conflict',
    410: 'Gone',
    411: 'Length Required',
    412: 'Precondition Failed',
    413: 'Request Entity Too Large',
    414: 'Request URI Too Long',
    415: 'Unsupported Media Type',
    416: 'Requested Range Not Satisfiable',
    417: 'Expectation Failed',
    418: "I'm a teapot",
    422: 'Unprocessable Entity',
    423: 'Locked',
    424: 'Failed Dependency',
    426: 'Upgrade Required',

    # Server Error
    500: 'Internal Server Error',
    501: 'Not Implemented',
    502: 'Bad Gateway',
    503: 'Service Unavailable',
    504: 'Gateway Timeout',
    505: 'HTTP Version Not Supported',
    507: 'Insufficient Storage',
    510: 'Not Extended',
}

#http 错误类
class HttpError(Exception):
    def __init__(self,code):
        super(HttpError,self).__init__()
        self.status = 'statuscode:%d -Detail Information: %s' % (code, _RESPONSE_STATUSES[code])
        logging.debug(self.status)
        if str(code).startswith('4') or str(code).startswith('5'):
            raise ValueError('http error---statuscode:%d -Detail Information: %s' % (code, _RESPONSE_STATUSES[code]))
    def __str__(self):
        return self.status


def _oauth_error(code):
    error = {
        'error': code,
        'error_description': _OAUTH_ERROR.get(str(code), 'unkown_error')
    }
    return json.dumps(error)

def get_token():
    result = requests.post(_URL, data=json.dumps(query_args), headers=_HEADERS).json()
    token = result.get(u'data').get(u'token',None)
    userId = str(result.get(u'data').get(u'userId',None))
    if token and userId:
        return (token,userId)

def token_required(func):
    @functools.wraps(func)
    def _wrapped_view(*args, **kwargs):
        result = requests.post(_URL, data=json.dumps(query_args), headers=_HEADERS).json()
        token = result.get(u'data').get(u'token',None)
        userId = str(result.get(u'data').get(u'userId',None))
        if token and userId:
            return func(*args, userId=userId,token=token)
        return _oauth_error('0001')
    return _wrapped_view

def get_without_token(arg):
    url = arg.split(',')[0]
    if not '{' in arg:
        params = dict()
    else:
        myarg = arg[arg.index('{'):]
        params = dict([a.split(':') for a in myarg.strip('{}').split(',')])
    def _decorate(func):
        @functools.wraps(func)
        def __deco(*args,**kwargs):
            r = requests.get(url,params)
            status_code = r.status_code
            HttpError(status_code)
            myjson = r.json()
            logging.debug(myjson)
            return func(*args,r=myjson)
        return __deco
    return _decorate

def get_whith_token(arg):
    url = arg.split(',')[0]
    if not '{' in arg:
        params = dict()
    else:
        myarg = arg[arg.index('{'):]
        params = dict([a.split(':') for a in myarg.strip('{}').split(',')])
    def _decorate(func):
        @functools.wraps(func)
        def __deco(*args,**kwargs):
            token,userId = get_token()
            headers = {'content-type': 'application/json', 'userId': userId, 'token': token} 
            r = requests.get(url,params,headers=headers)
            status_code = r.status_code
            HttpError(status_code)
            myjson = r.json()
            logging.debug(myjson)
            return func(*args,r=myjson)
        return __deco
    return _decorate

def post_with_token(arg):
    token,userId = get_token()
    url = str(arg.split(',')[0])
    if not '{' in arg:
        payload = dict()
    else:
        payload = eval(arg[arg.index('{'):])
    def _decorate(func):
        @functools.wraps(func)
        def __deco(*args,**kwargs):
            token,userId = get_token()
            headers = {'content-type': 'application/json', 'userId': userId, 'token': token}
            if payload == dict():
                r = requests.post(url, headers=headers)
            else:
                r = requests.post(url, json.dumps(payload), headers=headers)
            status_code = r.status_code
            HttpError(status_code)
            myjson = r.json()
            logging.debug(myjson)
            return func(*args,r=myjson)
        return __deco
    return _decorate

def delete_with_token(arg):
    token,userId = get_token()
    url = str(arg.split(',')[0])
    if not '{' in arg:
        payload = dict()
    else:
        payload = eval(arg[arg.index('{'):])
    def _decorate(func):
        @functools.wraps(func)
        def __deco(*args,**kwargs):
            token,userId = get_token()
            headers = {'content-type': 'application/json', 'userId': userId, 'token': token}
            if payload == dict():
                r = requests.delete(url, headers=headers)
            else:
                r = requests.delete(url, json.dumps(payload), headers=headers)
            status_code = r.status_code
            HttpError(status_code)
            myjson = r.json()
            logging.debug(myjson)
            return func(*args,r=myjson)
        return __deco
    return _decorate

def put_with_token(arg):
    token,userId = get_token()
    url = str(arg.split(',')[0])
    if not '{' in arg:
        payload = dict()
    else:
        payload = eval(arg[arg.index('{'):])
    def _decorate(func):
        @functools.wraps(func)
        def __deco(*args,**kwargs):
            token,userId = get_token()
            headers = {'content-type': 'application/json', 'userId': userId, 'token': token}
            if payload == dict():
                r = requests.put(url, headers=headers)
            else:
                r = requests.put(url, json.dumps(payload), headers=headers)
            status_code = r.status_code
            HttpError(status_code)
            myjson = r.json()
            logging.debug(myjson)
            return func(*args,r=myjson)
        return __deco
    return _decorate

    
#测试用
@token_required
def test_appscore(userId,token):
    print userId
    print token
    url = "http://comment.moxiancn.com/mo_comment/m1/appscore"
    payload = {
        "userId": userId,
        "appName": "1",
        "appVersion": "1",
        "telephoneType": "1",
        "moxianVersion": "1"
        }
    headers = {'content-type': 'application/json', 'userId': userId, 'token': token}
    r = requests.post(url, json.dumps(payload), headers=headers)
    print r.json()
    print r.status_code

if __name__ == '__main__':
    print get_token()






#!/user/bin/env python
#-*-coding:utf-8-*-

__author__ = 'gu.yanfeng'

from HTMLParser import HTMLParser
from htmlentitydefs import name2codepoint
import pickle
import sys
import os
reload(sys)
sys.setdefaultencoding('utf-8')
datapath = os.path.normpath(os.path.join(os.path.dirname(os.path.dirname(__file__)),'data'))

#get the data by tag name from html
class MyHTMLParser(HTMLParser):
	def __init__(self):
		fobj_init=open(r'%s/html_test_data.txt' % datapath,'w')
		fobj_init.write('')
		fobj_init.close()
		HTMLParser.__init__(self)
		self.flag=False
		self.type=''
	def handle_starttag(self,tag,attrs):
		if tag == 'td':
			self.flag = True
	def handle_data(self,data):
		if self.flag == True:
			if '+' not in data and data.strip() != "":
				fobj=open(r'%s/html_test_data.txt' % datapath,'a')
				pickle.dump(data,fobj)
				#fobj.write(data)
				fobj.close()
			self.flag = False

#get and return all of the error code html files
def gethtmlfiles():
	fileslist = []
	current_files = os.listdir(r'%s' % datapath)
	for filename in current_files:
		if '.htm' in filename:
			fileslist.append(os.path.join(r'%s/%s' % (datapath,filename)))
	if len(fileslist) > 0:
		return fileslist
	else:
		raise ValueError('No file is correct!')

#read html files and call MyHTMLParser to dump the data to txt
def gethtml():
	parser = MyHTMLParser()
	files = gethtmlfiles()
	for filename in files:
		html=open(r'%s' % filename)
		parser.feed(html.read())
		html.close()

#load the data from txt, return the error code and information
def load_error_txt(codestr):
	listcode=[]
	pk1f = open(r'%s/html_test_data.txt' % datapath)
	try:
		while True:
			data1=pickle.load(pk1f)
			if codestr in data1:
				listcode.append(data1.strip())
			if codestr in listcode:
				listcode.append(data1.strip())
				if len(listcode) > 3:
					break
	except :
		pass
	finally:
		pk1f.close()
	#print listcode[1:]
	return listcode[1:]
'''if __name__ == '__main__':
	gethtml()
	load_error_txt('mx2204003')'''
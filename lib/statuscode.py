#!/usr/bin/env python
# -*- coding:utf-8 -*-

__author__ = 'shi.xiaolong'

import json
import types
import re
import logging
import html_pickle

def parse_status(mydict):
    if type(mydict) == types.DictType:
        code = mydict.get(u'code','')
        if not code.strip():
            if mydict.get(u'result','') == False:
                logging.debug('code is returned None!--%s' % mydict)
                raise ValueError('code is returned None!--%s' % mydict)
            elif mydict.get(u'result') == True:
                logging.error("WARNING : the code is none but result is true! please check!")
        else:
            html_pickle.gethtml()
            error_status = html_pickle.load_error_txt(str(code))
            if len(error_status) == 0:
                logging.error('ERROR : the code:%s is not found in html_test_data.txt' % code)
                #raise ValueError('the code:%s is not found in html_test_data.txt' % code)
            else:
                if code == 'mx0000000':
                    logging.debug('code:%s,Business:%s,Details:%s' % (error_status[0],error_status[1],error_status[2]))
                else:
                    logging.warning('code:%s,Business:%s,Details:%s' % (error_status[0],error_status[1],error_status[2]))
    else:
        logging.error('Type Error,the type from inteface returned a:%s type,not dict' % type(mydict) )
        raise ValueError('Type Error,the type from inteface returned a:%s type,not dict' % type(mydict) )
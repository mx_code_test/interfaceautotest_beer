#!/usr/bin/env python
# -*- coding:utf-8 -*-

####################################
#  Version V1.0
#  Date:2015/06/09
#  Author shi.xiaolong
#  Usage:The testcase under testcase catalogue will be executed when you run Interface_test_all.py
####################################
import time
import unittest
import os
import sys
sys.path.append('./lib')
import HTMLTestRunner

filepath = os.path.abspath(os.path.normpath(os.path.dirname(__file__)))
test_file = os.path.normpath(os.path.join(filepath,"testcase"))

def creatsuitel():
    testunit=unittest.TestSuite()
    pattern = "*.py"
    discover=unittest.defaultTestLoader.discover(test_file,pattern,top_level_dir=None)
    for test_suite in discover:
        for test_case in test_suite:
            testunit.addTest(test_case)
    return testunit

if __name__ == '__main__':
    now = time.strftime("%Y-%m-%d-%H_%M_%S_",time.localtime(time.time()))
    filename = "result\\"  +  now + u"TestReport.html"
    fp = file(filename, 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(
    stream=fp,
    title=u'RESTAPI AutoTest Report',
    description=u'Detail Situation of Auto Testcase',
    )
    #run test case
    runner.run(creatsuitel())
    fp.close() #关闭生成的报告
    #执行发邮件
    #sendreport()
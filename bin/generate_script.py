#coding=utf-8
import xlrd
import os
import linecache
import re

InterFacefilepath = os.path.normpath(os.path.join(os.path.dirname(os.path.dirname(__file__)),'testcase'))

_RE_QUERY_URL = re.compile('(\w+)?.*')
_RE_DIGITAL_URL = re.compile('(.*)/(\w+)/(\d+)')

def creat_page_file(excel,tableName):
    u'''
    生成接口测试脚本
    '''
    #根据表名获取表对象
    table = excel.sheet_by_name(tableName)

    #获取总行数
    lineNum = table.nrows

    if lineNum == 0 or lineNum ==1:return

    #类名
    objectName = tableName + 'InterFace'

    #模块名
    moduleName = tableName.strip() + '.py'
    moduleName = os.path.normpath(os.path.join(InterFacefilepath,moduleName))

    #文档内容和导入内容
    classhead = "class %s(unittest.TestCase):\n" % (objectName)

    head = ["# -*- coding: utf-8 -*-\n\n","import unittest\n","import json\n","import requests\n","import sys\n","import logging\n","sys.path.append('..')\n",
            "from lib.statuscode import parse_status\n","from lib.log import *\n","from lib.method_decorator import *\n\n",classhead]


    #循环追加定位元素
    S = ''
    for i in range(1,lineNum):
        line = table.row_values(i)
        #过滤有空行的情况
        if line == '':pass
        #判断请求方式
        url = line[0]
        method = line[1]
        token = line[2]
        test = 'test'
        flag = int(line[4])
        params = line[3].strip('\n') if line[3] != 'None' else ''
        #trans for " to '
        if "\"" in params:
            params = params.replace("\"","\'")
        #remove \n for url & params
        url = url.strip('\n')
        if '\n' in params:
            params = params.replace("\n","")
        if '  ' in params:
            params = params.replace("  ","")
        params.strip('\n')
        comment = line[5]
        #method name
        func1 = url[url.rfind('/')+1:]
        if not params:
            myflag = 'Noparams_' + method
        else:
            myflag = 'Params_' + method
        if func1.isdigit():
            match = _RE_DIGITAL_URL.match(url)
            if match:func = myflag + '_' + match.group(2) + '_' + match.group(3)
        elif '?' in func1:
            match = _RE_QUERY_URL.match(func1)
            if match:func = myflag + '_' +match.group(1)
        else:
            func = myflag + '_'  + func1
            if func in S:
                func = func + str(i)
        #flag
        if str(flag).strip() != '1':
            test = 'Notest'
        #get_without_token
        if method.upper() == 'GET' and token.upper() == 'NO':
            S = S + '''    #%s\n    @get_without_token("%s,%s")\n\tdef %s_%s(self,r):\n\t\tparse_status(r)\n\n''' \
            % (comment,url,params,test,func)
        #get_with_token
        elif method.upper() == 'GET' and token.upper() == 'YES':
            S = S + '''    #%s\n    @get_whith_token("%s,%s")\n\tdef %s_%s(self,r):\n\t\tparse_status(r)\n\n''' \
            % (comment,url,params,test,func)
        #post_with_token
        elif method.upper() == 'POST' and token.upper() == 'YES':
            S = S + '''    #%s\n    @post_with_token("%s,%s")\n\tdef %s_%s(self,r):\n\t\tparse_status(r)\n\n''' \
            % (comment,url,params,test,func)
        #put_with_token
        elif method.upper() == 'PUT' and token.upper() == 'YES':
            S = S + '''    #%s\n    @put_with_token("%s,%s")\n\tdef %s_%s(self,r):\n\t\tparse_status(r)\n\n''' \
            % (comment,url,params,test,func)
        #delete_with_token
        elif method.upper() == "DELETE" and token.upper() == 'YES':
            S = S + '''    #%s\n    @delete_with_token("%s,%s")\n\tdef %s_%s(self,r):\n\t\tparse_status(r)\n\n''' \
            % (comment,url,params,test,func)

        Tail = '''if __name__ == '__main__':\n\tinitLogging('../log/output.log')\n\tunittest.main()'''
    S = S.expandtabs(4)
    Tail = Tail.expandtabs(4)
    with open(moduleName, 'w') as module:
        module.writelines(head)
    with open(moduleName, 'a') as module:
        module.write(S.encode('utf-8'))
    with open(moduleName, 'a') as module:
        module.write(Tail.encode('utf-8'))

if __name__ == '__main__':
    xlsfile = os.path.join(os.path.dirname(os.path.dirname(__file__)),'Interface.xls')
    #file_list = [x for x in os.listdir(xlspath) if x.endswith('.xls')]
    excel = xlrd.open_workbook(xlsfile)
    sheetNames = excel.sheet_names()
    #sheetNames = ['Moxian']
    print sheetNames
    for sheet in sheetNames:
            creat_page_file(excel,sheet)
@echo off
Set v=生成Page文件
title %v%
set tm1=%time:~0,2%
set tm2=%time:~3,2%
set tm3=%time:~6,2%
ECHO %date% %tm1%点%tm2%分%tm3%秒
echo -----------------------------------------------------
echo Are you sure to generate Interface AutoTestCase[Y,N]?
echo -----------------------------------------------------
choice /c YN /m "请选择" /T 30 /D N
if %errorlevel%==1 goto mm
if %errorlevel%==2  goto END

:mm
cd %cd%
start python F:\\interfaceautotest_beer\\bin\\generate_script.py
goto END

:END